import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index.vue'
import login from '../views/login.vue'


Vue.use(VueRouter)

const routes = [
  {
    path:"/",
    redirect:"/login"

  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/login.vue')
  },
  {
    path:"/index",
    name:'index',
    component: () => import(/* webpackChunkName: "index" */ '../views/index.vue'),
    children:[
      {
        path:'roles',
        name:'roles',
        component:() => import(/* webpackChunkName: "roles" */ '../views/roles/Roles.vue')
      },
      {
        path:'users',
        name:'users',
        component:() => import(/* webpackChunkName: "users" */ '../views/users/Users.vue')
      },
      {
        path:'rights',
        name:'rights',
        component:() => import(/* webpackChunkName: "rights" */ '../views/rights/Rights.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router

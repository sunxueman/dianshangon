import {http} from "./index"

export function login(data){
    return http("login","post",data)
}

export function getMenus(){
    return http('menus','GET')
}
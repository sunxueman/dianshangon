import axios from "axios"
import { Message } from 'element-ui';
import store from "../store"
import router from "../router"
const instance=axios.create({
    baseURL:"http://localhost:8888/api/private/v1/"
})
//请求拦截器
instance.interceptors.request.use((config)=>{
    if(store.state.token){
         config.headers['Authorization']=store.state.token
    }
    return config
},(error)=>{
    return Promise.reject(error)
})
//相应拦截器
instance.interceptors.response.use((response)=>{
    if(response.data.meta.status===400){
        router.push({
            name:'login'
        })
    }
    return response
},(error)=>{
    return Promise.reject(error)
})
export function http(url,method ,data,params){
    return new Promise((resolve,reject)=>{
        instance({
            url,
            method,
            data,
            params
        })
        .then(res=>{
            if(res.status>=200&&res.status<300||res.status===304){
                if(res.data.meta.status===200){                   
                    resolve(res.data.data)
                }else{
                    Message({
                        showClose: true,
                        message: res.data.meta.msg,
                        type: 'error'
                      })
                   console.log(reject(res)) 
                   reject(res.data.meta)
                }
            }else{
                Message({
                    showClose: true,
                    message: res.statusText,
                    type: 'error'
                  })
                reject(res)
            }
        }).catch(err=>{
            Message({
                showClose: true,
                message: err.message,
                type: 'error'
              })
            reject(err)
        })
    })
}
